//
//  Zapato.swift
//  Shoesbit
//
//  Created by Angello Luis on 12/8/18.
//  Copyright © 2018 devteam. All rights reserved.
//

import UIKit

class Zapato: NSObject {

    let id: String?
    let name: String?
    let descrition: String?
    let image:String?
    
    init(id: String?, nombre: String?, descripcion: String?, imagen: String?){
        
        self.id = id
        self.name = nombre
        self.descrition = descripcion
        self.image = imagen
        
        super.init()
    }
    
}
