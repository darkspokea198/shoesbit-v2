//
//  MapViewController.swift
//  Shoesbit
//
//  Created by Angello Luis on 12/5/18.
//  Copyright © 2018 devteam. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Alamofire

class MapViewController: UIViewController, CLLocationManagerDelegate {

    let manager = CLLocationManager()
    
    @IBOutlet weak var misTiendas: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

        if(checkForLocationServices()){
            manager.delegate = self
            manager.desiredAccuracy = kCLLocationAccuracyBest
            manager.requestWhenInUseAuthorization()
            manager.startUpdatingLocation()
        }
        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func checkForLocationServices() -> Bool {
        if CLLocationManager.locationServicesEnabled(){
            return true
        }else{
            return false
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        let latitud = location.coordinate.latitude
        let longitud = location.coordinate.longitude
        
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let miUbicacion = CLLocationCoordinate2DMake(latitud, longitud)
        let region = MKCoordinateRegion(center: miUbicacion, span: span)
        misTiendas.setRegion(region, animated: true)
        misTiendas.showsUserLocation = true
        
        self.agregarMarcador(titulo: "Tienda Importante", subtitulo: "Faster")
    }
    
    func agregarMarcador(titulo: String, subtitulo: String){
        
        let coordenadas = CLLocationCoordinate2D(latitude: -12.070349, longitude: -77.048286)
        let marcador = Marcador(titulo: titulo, subtitulo: subtitulo, coordenadas: coordenadas)
        
        misTiendas.addAnnotation(marcador)
    }
    
    func obtenerTiendas(){
         let urlString = "http://206.189.239.185/shoesbit/site/list-stores/"
        print(urlString)
        
        Alamofire.request(urlString, method:.get)
            .responseJSON(completionHandler: { response in
                switch response.result{
                case .success(let resultado) :
                    let search = resultado as? [NSDictionary]//[[String: Array]]
                    for Marcador in search! {
                        print(search)
                    }
                    break
                case .failure(let error):
                    print("eroooooorrrrr")
                    print(error)
                    break
                }
                
            })
    }
}
