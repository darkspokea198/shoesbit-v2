//
//  DetailShoesViewController.swift
//  Shoesbit
//
//  Created by Angello Luis on 12/5/18.
//  Copyright © 2018 devteam. All rights reserved.
//

import UIKit

class DetailShoesViewController: UIViewController {

    @IBOutlet weak var lblNombre: UILabel!
    @IBOutlet weak var imgShoes: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    
    var image = UIImage()
    var name = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblNombre.text = "you selected \(name) for note"
        imgShoes.image = image
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func onClickModels(_ sender: Any) {
        
    }
}
