//
//  ViewController.swift
//  Shoesbit
//
//  Created by Angello Luis on 12/5/18.
//  Copyright © 2018 devteam. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    var name = ["Angello", "Luis", "Rosario", "Melany"]
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellShoes = tableView.dequeueReusableCell(withIdentifier: "cellShoes", for: indexPath) as? cellShoesTableViewCell
        cellShoes?.lblNombre.text = name[indexPath.row]
        cellShoes?.imgShoes.image = UIImage(named: name[indexPath.row])
        return cellShoes!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "DetailShoesViewController") as? DetailShoesViewController
        vc?.image = UIImage(named: name[indexPath.row])!
        vc?.name = name[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    func cargarImages(){
        
    }
    
}

