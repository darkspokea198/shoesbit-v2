//
//  TipoZapato.swift
//  Shoesbit
//
//  Created by Angello Luis on 12/8/18.
//  Copyright © 2018 devteam. All rights reserved.
//

import UIKit

class TipoZapato: NSObject {

    let id: String?
    let name: String?
    let descritionn: String?
    
    init(id: String?, nombre: String?, descripcion: String?){
        
        self.id = id
        self.name = nombre
        self.descritionn = descripcion
        
        super.init()
    }
    
}
