//
//  Coordenadas.swift
//  Shoesbit
//
//  Created by Angello Luis on 12/8/18.
//  Copyright © 2018 devteam. All rights reserved.
//

import UIKit
import MapKit

class Coordenadas: NSObject, MKAnnotation  {
    
    let title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D
    
    init(titulo: String, subtitulo: String, coordenadas: CLLocationCoordinate2D){
        self.title = titulo
        self.subtitle = subtitulo
        self.coordinate = coordenadas
        
        super.init()
    }
    

}
